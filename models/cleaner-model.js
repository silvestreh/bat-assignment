const Sequelize = require('sequelize');

module.exports = function (sequelize) {
    const cleaner = sequelize.define('cleaners', {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        country_code: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                len: [2]
            }
        },
        latitude: {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        longitude: {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        gender: {
            type: Sequelize.ENUM('M', 'F'),
            allowNull: false
        },
        preferences: {
            type: Sequelize.ARRAY(Sequelize.STRING),
            allowNull: false
        }
    }, {
        freezeTableName: true
    });

    cleaner.sync();

    return cleaner;
};
