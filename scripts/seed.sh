#!/bin/bash

echo "Type the name of the database you'd like to seed:"
read dbname

if psql -lqt | cut -d \| -f 1 | grep -qw $dbname; then
    echo "Importing mock data…"
    psql $dbname < mock-data.sql &> /dev/null
    echo "Done!"
else
    createdb $dbname
    echo "Importing mock data…"
    psql $dbname < mock-data.sql &> /dev/null
    echo "Done!"
fi

