require('console.table');
const gulp = require('gulp');
const dbSettings = require('./config/db-settings');
const Sequelize = require('sequelize');
const db = new Sequelize(dbSettings.dbName, dbSettings.user, dbSettings.password, {
    host: dbSettings.host,
    dialect: 'postgres',
    logging: false
});
const Cleaner = require('./models/cleaner-model')(db);
const filterNearby = require('./src/filter-nearby');
const distance = require('./config/distance');
const validGeo = require('./helper/validate-geo');
const validGender = require('./helper/validate-gender');
const argv = require('yargs').argv;

gulp.task('get-matches', () => {
    // Exit if no geo argument is provided
    if (!argv.geo) {
        console.log('No geolocation provided. Exiting…');
        process.exit(1);
    }

    // Exit if no country argument is provided
    if (!argv.country) {
        console.log('No country provided. Exiting…');
        process.exit(1);
    }

    // Check wether geo is valid
    if (!validGeo(argv.geo)) {
        console.log('Invalid geo data. You should provide a comma separated latitude and longitude. Exiting…');
        process.exit(1);
    }

    // Parse the geo argument and create an object out of it
    const poi = {
        latitude: parseFloat(argv.geo.split(',')[0]),
        longitude: parseFloat(argv.geo.split(',')[1])
    };

    // Build the basic query
    const query = {
        raw: true,
        where: {
            country_code: argv.country.toUpperCase()
        }
    };

    // Add preferences in the query if it's provided as an argument
    if (argv.preferences && typeof argv.preferences === 'string') {
        query.where.preferences = { $contains: argv.preferences.split(',') };
    }

    // Add gender in the query if it's provided as an argument
    if (validGender(argv.gender)) {
        query.where.gender = argv.gender.toUpperCase();
    }

    // Find All records
    Cleaner.findAll(query).then((cleaners) => {
        const results = cleaners.filter((cleaner) => {
            // I'll delete these props as they don't add any relevant info
            delete cleaner.createdAt;
            delete cleaner.updatedAt;

            // Return only those that are nearby
            return filterNearby(cleaner, poi, distance[argv.country.toLowerCase()]);
        });

        if (results.length) {
            console.table(results);
        } else {
            console.log('No matches found');
        }

        process.exit(0);
    });
});
