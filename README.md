# Book a Tiger

> An assignment for backend developers for Book a Tiger

![](http://i.imgur.com/M3EvZUq.png)

## Requirements

* Node
* NPM
* A PostgreSQL database

## Installation

This *"project"* is extremely simple. There's not a lot going on, just fire up a terminal and run

```shell
cd path/to/bat-assignment
npm install
```

To seed the database you can run the `seed` NPM task. This will prompt for the database name, make sure is the same in `config/db-settings.json`.

```shell
npm run seed
```

This will be ran automatically when the install script finishes.

In `config/db-settings.json` you can set up your DB credentials. In `config/distance.json` you can tweak the search radius for each country. All distances are expressed in kilometers.

## Usage

Open a terminal emulator and run

```shell
gulp get-matches --country=<code> --geo=<lat>,<lng> --preferences=<preferences> --gender=<m|f>
```

## Caveats

The solution for the algorithm that calculates distances between points assumes the earth is flat (I'm truly sorry Ferdinand Magellan) and because of that it isn't very precise. But given the short distances that the spec requires to consider, it should be good enough.

Also, this implementation comes at a cost: scaling. Given that the solution is written in JavaScript, filtering the results won't be as efficient as doing it with a PSQL query. Unfortunately, my PSQL knowledge is as limited as the free time I have to learn the query language, but I'm well aware of the shortcomings of the proposed solution.
