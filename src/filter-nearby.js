/**
 * Filter Nearby
 *
 * @param {Object} cleaner    A Cleaner object with latitude and longitude properties
 * @param {Object} order      An Order object with latitude and longitude properties
 * @param {Number} km         Distance to `order` location in KM
 *
 * @returns {Boolean}         Returns `true` if a point is within a radius
 */

module.exports = function (cleaner, order, km) {
    const ky = 40000 / 360;
    const kx = Math.cos(Math.PI * order.latitude / 180.0) * ky;
    const dx = Math.abs(order.longitude - cleaner.longitude) * kx;
    const dy = Math.abs(order.latitude - cleaner.latitude) * ky;

    return Math.sqrt(dx * dx + dy * dy) <= km;
};
