function isFloat(number) {
    return !isNaN(parseFloat(number));
}

module.exports = function (geo) {
    if (typeof geo !== 'string') return false;

    const coordinates = geo.split(',');

    if (coordinates.length !== 2) return false;
    if (!isFloat(coordinates[0]) || !isFloat(coordinates[1])) return false;

    return true;
};
